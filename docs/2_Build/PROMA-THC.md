# Installation and configuration

## Torch height control with Proma and GRBL
- The Proma SD is what you have to order when you work with the grbl. To explain the process a little better since you are gonna set the cnc, with the grbl there is no way to get feedback from the THC.
- So the way the Proma SD works, is that it interferes between the Z motor driver and the grbl board, and it adjusts the Z signals on its own. At the end of the cut the board has no clue where the Z is since the THC most likely adjusted the Z during the cut.
- This on its own its not an issue, since after the cut, You raise the z to a safe height, move to the beginnging of the cut, and re-zero the Z with the zero-ing process,

## The traditional way with the non-grbl boards, is a little different:
The THC, (the Proma 150 for example) instead of adjusting the Z on its own, sends signals to the board about adjusting the Z. Then the board sends the signal to the Z motor driver. That way, the board knows exactly were the Z is everytime. The big advantage with this method is hiding in the details. It is that the board has the ability to ignore the THC signals.
Why should it do that? Well, in cutting small diameter holes, the common practice is to slow down the torch around 60%! This is done so the hole will be straight and clean and not tapered. But by slowing down, more material is removed, the THC will read higher voltage and it will think that the torch is too high so it will lower the Z, causing a probable collision. The problem is less intense with thin materials where the torch moves fast, and a recipe for a failed cut for thicker metals.

The way I deal with this problem? I set my Proma SD so it will not interfere for the first 3-4 secs after it detects an Arc. That way the small hole is cut without any "corrections".

I wanted to tell you that because you said thats a shop and they may like the extra control they have over the THC. In reality, my workaround works just fine, but if for some reason they want to turn off the THC during the cut the would be able to. Other than the small holes, I cant think any major reason. Maybe a minor one is on small angles on external cut one could slow down the speed during the corner, but its not that crucial as the small holes.

But the more control you have the better the quality of the final metal piece will be.

So because the THC costs a lot, you have to make a decision as to if you are going to stick with the grbl and the Proma SD, or you want another controller with and get the Proma 150
Moneywise the grbl approach is going to cost less.
